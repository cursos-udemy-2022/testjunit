package com.curso.junit.com.app;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class CalculadoraTest {

    @Mock
    CalculadoraGoogle calculadoraGoogle;

    @InjectMocks
    Calculadora calculadora;

    @BeforeEach
    public void init(){
        MockitoAnnotations.initMocks(this);
        when(calculadoraGoogle.sumar(5, 5)).thenReturn(10);
    }

    @Test
    public void sumaGoogleTest() {

        assertEquals(10, calculadora.sumaGoogle(5, 5));
    }
}