package com.curso.junit.com.app.controller;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ControllerTest {

    @Test
    public void welcomeTest(){
        Controller controller = new Controller();
        String[] paramArray = null;
        String resultActual = "El array esta vacio";
        String resultReal = controller.welcome(paramArray);
        assertEquals(resultActual, resultReal);
    }

    @Test
    public void welcomeArraySinDatosTest(){
        Controller controller = new Controller();
        String[] paramArray = new String[3];
        String resultActual = "param[0]null\n" + "param[1]null\n" + "param[2]null\n";
        String resultReal = controller.welcome(paramArray);
        assertEquals(resultActual, resultReal);
    }

    @Test
    public void welcomeArrayConDatosTest(){
        Controller controller = new Controller();
        String[] paramArray = new String[]{"java","desde", "0"};
        String resultActual = "param[0]java\n" + "param[1]desde\n" + "param[2]0\n";
        String resultReal = controller.welcome(paramArray);
        assertEquals(resultActual, resultReal);
    }

}