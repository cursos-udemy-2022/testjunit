package com.curso.junit.com.app.suma;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SumaTest {

    Suma suma = new Suma();

    @BeforeEach
    public void before(){
        System.out.println("before");
    }

    @AfterEach
    public void after(){
        System.out.println("after");
    }

    @Test
    public void sumaTest(){
        System.out.println("sumatest");
        int sumTest = suma.suma(1, 1);
        int resultado = 2;
        assertEquals(resultado, sumTest);
    }

}