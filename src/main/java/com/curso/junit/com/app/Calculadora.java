package com.curso.junit.com.app;

import org.springframework.beans.factory.annotation.Autowired;

public class Calculadora {

    @Autowired
    CalculadoraGoogle calculadoraGoogle;

    public int sumaGoogle(int x, int y){
        return calculadoraGoogle.sumar(x, y);
    }
}
