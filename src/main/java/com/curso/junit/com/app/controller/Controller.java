package com.curso.junit.com.app.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@org.springframework.stereotype.Controller
public class Controller {

    @PostMapping("/welcome")
    @ResponseBody
    public String welcome(@RequestParam(required = false, name = "param") String[] paramArray){
        String msg = "";
        int i =0;
        if (paramArray == null){
            msg = "El array esta vacio";
        }else {
            for (String paramArrayItem : paramArray){
                msg += "param[" + i + "]" + paramArrayItem + "\n";
                i++;
            }
        }
        return msg;
    }
}
